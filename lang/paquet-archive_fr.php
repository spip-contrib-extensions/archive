<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/archive.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'archive_description' => 'Ce plugin ajoute le statut "archive" aux objets.',
	'archive_nom' => 'Archive',
	'archive_slogan' => 'Ajouter le statut "archive" aux objets'
);
