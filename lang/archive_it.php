<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/archive?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'arch_auto' => 'Archiviazione automatica:',
	'articles_archives' => 'Articoli archiviati:',

	// C
	'choix_rubrique' => 'Scegli la sezione da utilizzare:',
	'configurer_archive' => 'Configura il plugin Archive',

	// D
	'doc_complete' => 'Documentazione completa',

	// I
	'info_article_archive' => 'Articolo archiviato',
	'info_defaut_desc' => 'Qui puoi configurare le impostazioni per l’archiviazione automatica',

	// J
	'jours' => 'giorno(i)',
	'jours_archive' => 'Archivia articoli più vecchi di:',

	// L
	'label_archiver_publier' => 'Gli archivi sono visibili nello spazio pubblico',
	'legend_arch_auto' => 'Impostazioni per l’archiviazione automatica',

	// N
	'nom_plugin' => 'Plugin Archive',
	'non' => 'No',

	// O
	'oui' => 'Si',

	// R
	'rappel_utilisation' => 'Promemoria su come usarlo:',
	'rubriques_archiver' => 'Sezioni da archiviare:',

	// T
	'texte_statut_archive' => 'archiviato',
	'titre_archive' => 'Plugin Archive',
	'titre_archives_rubrique' => 'L’archivio di questa rubrica:',

	// V
	'version' => 'Versione attuale:'
);
