<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-archive?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'archive_description' => 'This plugin adds the status "archive" to objects.',
	'archive_nom' => 'Archive',
	'archive_slogan' => 'Add status "archive" to objects'
);
