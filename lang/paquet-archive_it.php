<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-archive?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'archive_description' => 'Questo plugin aggiunte lo stato "archiviato" agli oggetti',
	'archive_nom' => 'Archivio',
	'archive_slogan' => 'Aggiungi lo stato "archiviato" agli oggetti'
);
