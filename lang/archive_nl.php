<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/archive?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'arch_auto' => 'Automatische archivering: ',
	'articles_archives' => 'Gearchiveerde artikelen:',

	// C
	'choix_rubrique' => 'Keuze van de te gebruiken rubriek:',
	'configurer_archive' => 'Plugin Archive configureren',

	// D
	'doc_complete' => 'Volledige documentatie',

	// I
	'info_article_archive' => 'Gearchiveerd artikel',
	'info_defaut_desc' => 'Hier kun je de parameters voor automatische archivering bepalen',

	// J
	'jours' => 'dag(en)',
	'jours_archive' => 'Archiveer artikelen van meer dan:',

	// L
	'label_archiver_publier' => 'Archieven zijn op de publieke site zichtbaar',
	'legend_arch_auto' => 'Parameters voor automatische archivering',

	// N
	'nom_plugin' => 'De plugin Archive',
	'non' => 'Nee',

	// O
	'oui' => 'Ja',

	// R
	'rappel_utilisation' => 'Hoe ook weer te gebruiken:',
	'rubriques_archiver' => 'Te archiveren rubrieken:',

	// T
	'texte_statut_archive' => 'gearchiveerd',
	'titre_archive' => 'De plugin Archive',
	'titre_archives_rubrique' => 'De archieven van deze rubriek',

	// V
	'version' => 'Huidige versie:'
);
