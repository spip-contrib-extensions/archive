# Plugin "Archive" version 2.0

Ajoute un statut "*Archive*" aux objets SPIP.

Cette version est une interpréation différente de la contribution éponyme présentée sur [SPIP-contrib](https://contrib.spip.net/Plugin-Archive).

Le code est réécrit dans presque sa totalité suite aux améliorations que permettent les nouvelles APIs de SPIP > 3.0.

## Fonctionnement

### Configuration

### Modification de l'interface d'édition

### Modification du comportement des boucles

### Archivage automatique des objets

## Compatibilité avec les anciennes versions

Un retrocompatbilité est proposée pour migrer depuis la contribution initiale. 
Il est à noter que le concept d'archive ```{archive}``` n'est pas maintenu. Cette implémentation considère les archives comme un statut en soit et non une information complémentaire au statut existant. 

Pour exploiter correctement ce plugin, les critères ```{archive}``` et ```{archive seulement}``` doivent être remplacés dans les squelettes par ```{statut?}``` et ```{statut = archive}```

### Alternative

Il existe un autre plugin proposant une gestion des archives qui peut s'apparenter à la contribution initiale. Il s'agit d'[archive_objet](https://git.spip.net/spip-contrib-extensions/archive_objet)

## Modifications et ajouts par rapport aux anciennes versions

* La notion d'archive devient un statut.